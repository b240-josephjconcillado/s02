<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    //
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        if(Auth::user()){
            // create a new Post Object from the post model
            $post = new Post;

            // define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = Auth::user()->id;
            $post->save();

            return "<p>Post Created successfully</p>";
        } else {
            return redirect('/login');
        }
    }

    public function index()
    {
        //get all posts from the database
        $posts = Post::all();
        return view('posts.index')->with('posts', $posts);
    }

    public function welcome()
    {
        $posts = Post::inRandomOrder()
                ->limit(3)
                ->get();
        return view('welcome')->with('posts', $posts); 
    }

}
