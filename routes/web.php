<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'welcome']);

//route for viewing the create posts page
Route::get('/posts/create', [PostController::class, 'create']);
//route for sending form data via POST request to the /posts endpoint
Route::post('/posts', [PostController::class, 'store']);
//route to return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
